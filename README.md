# k8s-nginx-pvc-pv

nginx kubernetes pvc, pv example

tested with docker desktop ce for windows Version 2.0.0.3 (31259)

- on windows share your desired local drive in docker desktop --> Settings --> Shared Drives
- create an index.html file (in this example f:\k8s\index.html)
- adapt path: "/f/k8s" in pv-windows.yaml where you have created your index.html
- docker build . -t nginx-pv:1.0 (deployment.yaml needs this tagged version)
- kubectl create -f pv-windows.yaml
- kubectl create -f pvc.yaml
- kubectl create -f deployment.yaml
- kubectl create -f nlb.yaml
- open browser on http://localhost
- change index.html content
- to see the new content simply reload http://localhost
